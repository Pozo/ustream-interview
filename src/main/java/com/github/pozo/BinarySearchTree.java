package com.github.pozo;

public class BinarySearchTree<T extends Comparable<T>> {

    private TreeNode<T> root;

    public void addElement(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element cant be 'null'");
        }

        if (root == null) {
            root = new TreeNode<>(null);
            root.setValue(element);
        } else {
            addElement(root, element);
        }
        // TODO ?addElement(element)
    }

    void addElement(final TreeNode<T> parent, final T element) {
        T parentValue = parent.getValue();
        int comparingResult = parentValue.compareTo(element);

        if (comparingResult == 0) { // equal
            // XXX same values are not allowed
        } else if (comparingResult < 0) { // positive
            if (parent.getRight() == null) {
                TreeNode<T> rightNode = new TreeNode<>(parent);
                rightNode.setValue(element);
                parent.setRight(rightNode);
            } else {
                addElement(parent.getRight(), element);
            }

        } else if (comparingResult > 0) { // negative
            if (parent.getLeft() == null) {
                TreeNode<T> leftNode = new TreeNode<>(parent);
                leftNode.setValue(element);
                parent.setLeft(leftNode);
            } else {
                addElement(parent.getLeft(), element);
            }
        } else {
            throw new IllegalArgumentException("Ambiguous result during compare!");
        }
    }

    TreeNode<T> getRootTreeNode() {
        if (root == null) {
            throw new IllegalArgumentException("Root is null");
        } else {
            return root;
        }
    }

    T getRootElement() {
        if (root == null) {
            throw new IllegalArgumentException("Root is null");
        } else {
            return root.getValue();
        }
    }
}
