package com.github.pozo;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class NodeTest {

    @Test
    public void testChainCall() {
        // GIVEN
        TreeNode node = new TreeNode(null);
        TreeNode firstChild = new TreeNode(node);

        // WHEN
        TreeNode firstChildParent = firstChild.getParent();

        // THEN
        assertNotNull(firstChildParent);

    }
}
