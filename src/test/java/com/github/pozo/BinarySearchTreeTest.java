package com.github.pozo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BinarySearchTreeTest {

    @Test(expected = IllegalArgumentException.class)
    public void testAddElementAsRoot_WhenRootHasNoValue() {
        // GIVEN
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        // WHEN
        // THEN
        bst.getRootElement();
    }

    @Test
    public void testAddFirstElement() {
        // GIVEN
        final Integer firstValue = 666;
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        // WHEN
        bst.addElement(firstValue);

        // THEN
        Integer rootValue = bst.getRootElement();
        assertNotNull(rootValue);
        assertEquals(firstValue, rootValue);
    }

    @Test
    public void testFlatGraphWithSeveralElements() {
        // GIVEN
        final Integer firstValue = 5;
        final Integer secondValue = 3;
        final Integer thirdValue = 12;

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        // WHEN
        bst.addElement(firstValue);
        bst.addElement(secondValue);
        bst.addElement(thirdValue);

        // THEN
        final TreeNode<Integer> rootTreeNode = bst.getRootTreeNode();
        Integer rootValue = rootTreeNode.getValue();
        Integer rootLeftTreeNodeValue = rootTreeNode.getLeft().getValue();
        Integer rootRightTreeNodeValue = rootTreeNode.getRight().getValue();

        assertEquals(firstValue, rootValue);
        assertEquals(secondValue, rootLeftTreeNodeValue);
        assertEquals(thirdValue, rootRightTreeNodeValue);
    }

    @Test
    public void testGraphWithSeveralElements() {
        // GIVEN
        final Integer firstValue = 5;
        final Integer secondValue = 3;
        final Integer thirdValue = 12;
        final Integer forthValue = 8;

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        // WHEN
        bst.addElement(firstValue);
        bst.addElement(secondValue);
        bst.addElement(thirdValue);
        bst.addElement(forthValue);

        // THEN
        final TreeNode<Integer> rootTreeNode = bst.getRootTreeNode();
        final TreeNode<Integer> firstRightNode = rootTreeNode.getRight();

        Integer rootValue = rootTreeNode.getValue();
        Integer rootLeftTreeNodeValue = rootTreeNode.getLeft().getValue();
        Integer rootRightTreeNodeValue = firstRightNode.getValue();
        Integer rootRightTreeNode_RightNodesValue = firstRightNode.getLeft().getValue();

        assertEquals(firstValue, rootValue);
        assertEquals(secondValue, rootLeftTreeNodeValue);
        assertEquals(thirdValue, rootRightTreeNodeValue);
        assertEquals(forthValue, rootRightTreeNode_RightNodesValue);
    }
}
